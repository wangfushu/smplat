package com.xiaoshuai.plat.vo.message.req;
/**
 * 图片消息
 * @author 宗潇帅
 * @修改日期 2014-7-11下午5:48:40
 */
public class ImageMessage  {
	//图片链接
	private String PicUrl;

	public String getPicUrl() {
		return PicUrl;
	}

	public void setPicUrl(String picUrl) {
		PicUrl = picUrl;
	}
	
}
