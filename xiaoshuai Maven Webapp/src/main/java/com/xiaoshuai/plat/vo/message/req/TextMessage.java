package com.xiaoshuai.plat.vo.message.req;
/**
 * 文本消息
 * @author 宗潇帅
 * @修改日期 2014-7-11下午5:46:57
 */
public class TextMessage extends BaseMessage {
	//消息内容
	private String Content;

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}
	
}
